package com.ynov.android.config;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.ynov.android.utils.Views;
import org.jongo.Jongo;
import org.jongo.Mapper;
import org.jongo.marshall.jackson.JacksonMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by tom on 25/07/2016.
 */
@Configuration
@ConfigurationProperties(prefix = "cors")
public class MongoConfig {


    private String[] allowedOrigins;

    public String[] getAllowedOrigins() {
        return allowedOrigins;
    }

    public void setAllowedOrigins(String[] allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }

    @Bean(name = "JongoObjectMapper")
    public Mapper jongoObjectMapper(){
        return new JacksonMapper.Builder()
                .registerModule(new JodaModule())
                .enable(MapperFeature.AUTO_DETECT_GETTERS)
                .withView(Views.Private.class)
                .build();
    }

    @Bean
    public Jongo jongo(@Value("${jongo.host}") String host,
                       @Value("${jongo.port}") int port,
                       @Value("${jongo.dbName}") String dbName,
                       @Qualifier("JongoObjectMapper") Mapper mapper) throws Exception {
        DB db = new MongoClient(host, port)
                .getDB(dbName);

        return new Jongo(db, mapper);
    }

    @Bean(name = "media")
    public GridFS gridFS(Jongo jongo) throws Exception {
        GridFS res = new GridFS(jongo.getDatabase(),"files");
        return res;
    }

    /*@Bean(name = "torrents")
    public GridFS torrentGridFS(Jongo jongo) throws Exception {
        GridFS res = new GridFS(jongo.getDatabase(),"torrents");
        return res;
    }*/

    /*@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                //registry.addMapping("/").allowedOrigins("http://www.xwinga.local");
                registry.addMapping("/**").allowedOrigins(allowedOrigins);
            }
        };
    }*/

    @Bean
    public ExecutorService executorService(@Value("${execution.maxThreads}") int maxThreads){
        return Executors.newFixedThreadPool(maxThreads);
    }

}
