package com.ynov.android.security;

import com.ynov.android.annotation.Secured;
import com.ynov.android.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;


/**
 * Created by tom on 19/10/2016.
 */
@Service
public class SecurityInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod method = (HandlerMethod) handler;
        checkType(method);
        checkMethod(method.getMethod());
        return true;
    }

    private void checkMethod(Method method) {
        for(Annotation annotation: method.getAnnotations()){
            if(annotation instanceof Secured){
                checkRolesAgainstCurrentUser(((Secured)annotation).roles(), ((Secured)annotation).strategy());
            }
        }
    }

    private void checkType(HandlerMethod method) {
        for(Annotation annotation: method.getBeanType().getAnnotations()){
            if(annotation instanceof Secured){
                checkRolesAgainstCurrentUser(((Secured)annotation).roles(), ((Secured)annotation).strategy());
            }
        }
    }

    private void checkRolesAgainstCurrentUser(Role[] roles, RoleStrategy strategy) {
        Optional<User> user = Principal.current();
        if(!user.isPresent()){
            throw new SecuredResourceException();
        }
        List<Role> roleList = newArrayList(roles);
        if(roleList.contains(Role.ALL)){
            return;
        }
        List<Role> userRoles = user.get().getRoles();
        strategy.check(userRoles, roleList);
    }
}
