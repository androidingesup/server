package com.ynov.android.security;


import com.ynov.android.domain.User;

import java.util.Optional;

/**
 * Created by tom on 19/10/2016.
 */
public class Principal {

    private static ThreadLocal<Optional<User>> current = new ThreadLocal<Optional<User>>();

    public static Optional<User> current(){
        Optional<User> curr = current.get();
        return curr == null ? Optional.empty() : curr;
    }

    public static void setCurrent(Optional<User> userOptional){
        current.set(userOptional);
    }

    public static void clear(){
        current.remove();
    }

}
