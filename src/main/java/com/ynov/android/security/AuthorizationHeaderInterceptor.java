package com.ynov.android.security;

import com.ynov.android.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Created by tom on 19/10/2016.
 */
@Service
public class AuthorizationHeaderInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String authorization = request.getHeader("Authorization");
        if(authorization != null){
            Principal.setCurrent(Optional.ofNullable(userService.getUserForToken(authorization)));
        }
        return true;
    }
}
