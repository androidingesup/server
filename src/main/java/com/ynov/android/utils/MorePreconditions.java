package com.ynov.android.utils;

import java.util.Optional;

/**
 * Created by tom on 12/01/2017.
 */
public class MorePreconditions {
    public static <T> T checkPresent(Optional<T> optional, RuntimeException e){
        return optional.orElseThrow(() -> e);
    }
}
