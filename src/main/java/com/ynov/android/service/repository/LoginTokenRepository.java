package com.ynov.android.service.repository;

import com.ynov.android.domain.LoginToken;
import org.apache.commons.logging.Log;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by tom on 12/01/2017.
 */
@Service
public class LoginTokenRepository extends AbstractRepository<LoginToken> {
    public void revokeForToken(String token) {
        collection.remove("{ token: # }", token);
    }

    public Optional<LoginToken> findToken(String authorization) {
        return Optional.ofNullable(collection.findOne("{ token: # }", authorization).as(LoginToken.class));
    }
}
