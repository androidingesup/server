package com.ynov.android;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by tom on 10/01/2017.
 */
@SpringBootApplication
public class Server {

    public static void main(String[] args){
        SpringApplication.run(Server.class, args);
    }

}
