package com.ynov.android.rest;

import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tom on 12/01/2017.
 */
@RestController
@RequestMapping("/hello-world")
public class HelloWorldController {

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String render(){
        return "Hello world from server "+ DateTime.now().toDate().toString();
    }

}
