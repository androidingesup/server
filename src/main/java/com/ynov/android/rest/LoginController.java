package com.ynov.android.rest;

import com.ynov.android.domain.LoginToken;
import com.ynov.android.domain.User;
import com.ynov.android.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tom on 12/01/2017.
 */
@RestController
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public LoginToken login(@RequestBody User user) {
        return userService.findUserAndLogin(user);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout(@RequestBody LoginToken token) {
        userService.logout(token.getToken());
    }
}
