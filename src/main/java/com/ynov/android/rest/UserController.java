package com.ynov.android.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.ynov.android.domain.User;
import com.ynov.android.service.UserService;
import com.ynov.android.utils.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by tom on 12/01/2017.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    @JsonView(Views.Public.class)
    public User create(@RequestBody @Valid @JsonView(Views.Private.class) User user){
        return userService.create(user);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
    public User update(@PathVariable String userId, @RequestBody @Valid User user){
        return userService.update(userId, user);
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public User update(@PathVariable String userId){
        return userService.userWithId(userId);
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    @JsonView(Views.Public.class)
    public Iterable<User> list(){
        return userService.all();
    }

}
