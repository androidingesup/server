package com.ynov.android.domain.core;

import org.jongo.marshall.jackson.oid.MongoObjectId;

/**
 * Created by tom on 25/07/2016.
 */
public class UserLoggableEntity extends TimestampableEntity {

    @MongoObjectId
    private String creator;
    @MongoObjectId
    private String modifier;

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
}
